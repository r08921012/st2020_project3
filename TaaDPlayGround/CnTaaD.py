

####### CnTaaD #############################
from __future__ import print_function    # (at top of module)
import sys


import platform 

'''
print (sys.argv)
print ("(^o^)")
'''

# to pack everythin in one, go the directory and execute the following. 
#   pyinstaller -y --onefile .\CnTaaD.py -


if sys.version_info[0] < 3: # Python 2 and 3:

    #  to execute the file, type in the following command in powershell: 
    #  % python CnTaaDPackage.py build_ext --inplace
    import future        # pip install future
    import builtins      # pip install future
    import past          # pip install future
    import six           # pip install six


#-------------------------------------------------------------------------------
# Name:        
# Purpose:
#
# Author:      Farn Wang
#
# Created:     08/01/2015
# Copyright:   (c) Farn Wang 2015
# Licence:     <your licence>

# File for registration and login server. 
# C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\Lib\site-packages\security_jigsaw
# 
#-------------------------------------------------------------------------------

import os, sys
# from sys import platform as _platform

import time as simpleTime
from datetime import datetime, time 

#==========================================================
# from distutils.core import setup # this is for python 3.4
# the following are for python 2.7
if sys.version_info[0] < 3: 
    from setuptools import setup
    from setuptools import Extension
else: 
    from distutils.core import setup
    from distutils.extension import Extension

import traceback 

__sysRealPath = os.path.realpath(__file__)
print ("CnTaaD realPath: ", __sysRealPath)

def queryRealPath(): 
    global __sysRealPath 
    
    return __sysRealPath 

cRoot = os.path.dirname(__sysRealPath)
srcDir = os.path.join(cRoot, "src")

if srcDir not in sys.path: 
    sys.path.append(srcDir)

if cRoot not in sys.path: 
    sys.path.append(cRoot)

if __name__ == '__main__':
    subs = os.listdir(srcDir)
    os.chdir(srcDir)
    if platform.system() == "Darwin":
        for k in subs: 
            if k.startswith('CnTestingTask.') and k.endswith('.so.update'): 
                kt = len(k) - 7
                kn = k[:kt]
                try: 
                    os.remove(kn)
                except: 
                    print("file ", kn, " does not exists!")
                os.rename(k, kn)
                break 
    else: 
        for k in subs: 
            if k.startswith('CnTestingTask.') and k.endswith('.pyd.update'): 
                kt = len(k) - 7
                kn = k[:kt]
                try: 
                    os.remove(kn)
                except: 
                    print("file ", kn, " does not exists!")
                os.rename(k, kn)
                break 

    import CnTestingTask 
    CnTestingTask.CnTestingTask_CnTaaDTask()

